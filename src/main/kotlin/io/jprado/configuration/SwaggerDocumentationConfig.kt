package io.jprado.configuration

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

import springfox.documentation.builders.ApiInfoBuilder
import springfox.documentation.builders.RequestHandlerSelectors
import springfox.documentation.service.ApiInfo
import springfox.documentation.service.Contact
import springfox.documentation.spi.DocumentationType
import springfox.documentation.spring.web.plugins.Docket
import java.time.LocalDate
import java.time.LocalTime

@javax.annotation.Generated(value = "class io.swagger.codegen.languages.SpringCodegen", date = "2017-02-11T19:58:36.526+11:00")

@Configuration
open class SwaggerDocumentationConfig {

    open internal fun apiInfo(): ApiInfo {
        return ApiInfoBuilder()
                .title("Account API")
                .description("A Tenantis an account (ledger) for tracking the amount of rent paid (in Australian dollars only) by a tenantâ€™s lease. ")
                .license("")
                .licenseUrl("")
                .termsOfServiceUrl("")
                .version("1.0.0")
                .contact(Contact("", "", ""))
                .build()
    }

    @Bean
    open fun customImplementation(): Docket {
        return Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.basePackage("io.jprado.api"))
                .build()
                .directModelSubstitute(LocalDate::class.java, java.sql.Date::class.java)
                .directModelSubstitute(LocalTime::class.java, java.util.Date::class.java)
                .apiInfo(apiInfo())
    }

}
