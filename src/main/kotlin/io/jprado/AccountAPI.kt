package io.jprado

import com.fasterxml.jackson.module.kotlin.KotlinModule
import org.springframework.boot.CommandLineRunner
import org.springframework.boot.ExitCodeGenerator
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.ComponentScan
import springfox.documentation.swagger2.annotations.EnableSwagger2

@SpringBootApplication
@EnableSwagger2
@ComponentScan(basePackages = arrayOf("io.jprado"))
open class AccountAPI : CommandLineRunner {

    @Bean
    open fun kotlinModule() = KotlinModule()

    @Throws(Exception::class)
    override fun run(vararg arg0: String) {
        if (arg0.size > 0 && arg0[0] == "exitcode") {
            throw ExitException()
        }
    }

    internal inner class ExitException : RuntimeException(), ExitCodeGenerator {

        override fun getExitCode(): Int {
            return 10
        }

    }

    companion object {
        private val serialVersionUID = 1L
        @Throws(Exception::class)
        @JvmStatic fun main(args: Array<String>) {
            SpringApplication(AccountAPI::class.java).run(*args)
        }
    }
}