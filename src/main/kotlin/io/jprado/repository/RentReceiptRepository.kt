package io.jprado.repository

import io.jprado.model.RentReceipt
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository
import java.time.LocalDateTime

/**
 * Created by jpprado on 12/2/17.
 */
@Repository
interface RentReceiptRepository : CrudRepository<RentReceipt, Long> {

    fun findByCreatedDateTimeAfter(createdDateTime: LocalDateTime): List<RentReceipt>
}