package io.jprado.repository

import io.jprado.model.Tenant
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository

/**
 * Created by jpprado on 12/2/17.
 */
@Repository
interface TenantRepository : CrudRepository<Tenant, Long> {

}