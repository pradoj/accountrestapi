package io.jprado.api

import io.swagger.annotations.*
import io.jprado.model.RentReceipt
import io.jprado.model.Tenant
import io.jprado.model.dto.RentReceiptsResponseDTO
import io.jprado.model.dto.TenantDTO
import io.jprado.model.dto.TenantResponseDTO
import io.jprado.model.dto.TenantsResponseDTO
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@javax.annotation.Generated(value = "class io.swagger.codegen.languages.SpringCodegen", date = "2017-02-11T19:58:36.526+11:00")

@Api(value = "tenant", description = "the tenant API")
interface TenantApi  {

    @ApiOperation(value = "Create a new tenant", notes = "Create a new tenant.", response = Void::class, tags = arrayOf("tenant"))
    @ApiResponses(value = *arrayOf(ApiResponse(code = 405, message = "Invalid input", response = Void::class)))
    @RequestMapping(value = "/tenant", produces = arrayOf("application/json"), consumes = arrayOf("application/json"), method = arrayOf(RequestMethod.POST))
    fun tenantPost(

            @ApiParam(value = "Payload of a new tenant.", required = true) @RequestBody tenantPayload: TenantDTO

    ): ResponseEntity<TenantResponseDTO>


    @ApiOperation(value = "Get tenant by ID", notes = "Gets a single Tenant by its unique ID. ", response = Tenant::class, tags = arrayOf("tenant"))
    @ApiResponses(value = *arrayOf(ApiResponse(code = 200, message = "successful operation", response = Tenant::class), ApiResponse(code = 400, message = "Invalid ID supplied", response = Void::class), ApiResponse(code = 404, message = "Tenant not found", response = Void::class)))
    @RequestMapping(value = "/tenant/{tenantId}", produces = arrayOf("application/json"), method = arrayOf(RequestMethod.GET))
    fun tenantTenantIdGet(
            @ApiParam(value = "Unique id of the Tenant.", required = true) @PathVariable("tenantId") tenantId: Long?


    ): ResponseEntity<TenantResponseDTO>


    @ApiOperation(value = "Get tenant's receipts", notes = "Get tenant's receipts. ", response = RentReceipt::class, responseContainer = "List", tags = arrayOf("tenant", "receipt"))
    @ApiResponses(value = *arrayOf(ApiResponse(code = 200, message = "successful operation", response = RentReceipt::class), ApiResponse(code = 400, message = "Invalid ID supplied", response = Void::class), ApiResponse(code = 404, message = "Tenant not found", response = Void::class)))
    @RequestMapping(value = "/tenant/{tenantId}/receipts", produces = arrayOf("application/json"), method = arrayOf(RequestMethod.GET))
    fun tenantTenantIdReceiptsGet(
            @ApiParam(value = "Unique id of the Tenant.", required = true) @PathVariable("tenantId") tenantId: Long


    ): ResponseEntity<RentReceiptsResponseDTO>


    @ApiOperation(value = "List all  Tenants  which have a  Rent Receipt created within the last hours", notes = "List all  Tenants  which have a  Rent Receipt created within the last â€œNâ€ hours ", response = Tenant::class, responseContainer = "List", tags = arrayOf("tenant"))
    @ApiResponses(value = *arrayOf(ApiResponse(code = 200, message = "successful operation", response = Tenant::class), ApiResponse(code = 400, message = "Invalid hour supplied", response = Tenant::class), ApiResponse(code = 404, message = "No receipts found", response = Tenant::class)))
    @RequestMapping(value = "/tenant/filterByRentReceiptCreatedWithinHours", produces = arrayOf("application/json"), method = arrayOf(RequestMethod.GET))
    fun tenantListRentReceiptCreatedWithinHours(@ApiParam(value = "Number of last hours to search.", required = true) @RequestParam(value = "hours", required = true) hours: Long


    ): ResponseEntity<TenantsResponseDTO>
}
