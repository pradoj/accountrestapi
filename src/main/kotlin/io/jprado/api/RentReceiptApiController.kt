package io.jprado.api

import io.swagger.annotations.ApiParam
import io.jprado.model.dto.RentReceiptCreateDTO
import io.jprado.model.dto.RentReceiptResponseDTO
import io.jprado.service.RentReceiptService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController


@javax.annotation.Generated(value = "class io.swagger.codegen.languages.SpringCodegen", date = "2017-02-11T19:58:36.526+11:00")

@RestController
class RentReceiptApiController @Autowired constructor(val rentReceiptService: RentReceiptService): RentReceiptApi {

    override fun rentReceiptPost(

            @ApiParam(value = "Unique id of the Tenant.", required = true) @RequestBody rentReceiptPayload: RentReceiptCreateDTO

    ): ResponseEntity<RentReceiptResponseDTO> {
        val rentReceiptResponseDTO = RentReceiptResponseDTO(rentReceiptService.create(rentReceiptPayload))

        if (rentReceiptResponseDTO.rentReceipt == null){
            return ResponseEntity(HttpStatus.BAD_REQUEST)
        }else {
            return ResponseEntity(rentReceiptResponseDTO, HttpStatus.OK)
        }
    }

}
