package io.jprado.api

@javax.annotation.Generated(value = "class io.swagger.codegen.languages.SpringCodegen", date = "2017-02-11T19:58:36.526+11:00")
open class ApiException(private val code: Int, msg: String) : Exception(msg)
