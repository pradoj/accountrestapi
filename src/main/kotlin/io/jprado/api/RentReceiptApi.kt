package io.jprado.api

import io.swagger.annotations.*
import io.jprado.model.dto.RentReceiptCreateDTO
import io.jprado.model.dto.RentReceiptResponseDTO
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod

@javax.annotation.Generated(value = "class io.swagger.codegen.languages.SpringCodegen", date = "2017-02-11T19:58:36.526+11:00")

@Api(value = "rentReceipt", description = "the rentReceipt API")
interface RentReceiptApi {

    @ApiOperation(value = "Create a new rent receipt", notes = "Create a new rent receipt. Rent Receipts  can be created for a  Tenant w  ith the following rules: â— The Tenantâ€™s  paid-to date  should be automatically advanced based on the amount of rent paid â— The  paid-to date  must be advanced in whole weeks only (  rent period) and any remainder should be stored as  rent credit  which is automatically used for the next rent receip ", response = Void::class, tags = arrayOf("receipt"))
    @ApiResponses(value = *arrayOf(ApiResponse(code = 405, message = "Invalid input", response = Void::class)))
    @RequestMapping(value = "/rentReceipt", produces = arrayOf("application/json"), consumes = arrayOf("application/json"), method = arrayOf(RequestMethod.POST))
    fun rentReceiptPost(

            @ApiParam(value = "Unique id of the Tenant.", required = true) @RequestBody rentReceiptPayload: RentReceiptCreateDTO

    ): ResponseEntity<RentReceiptResponseDTO>

}
