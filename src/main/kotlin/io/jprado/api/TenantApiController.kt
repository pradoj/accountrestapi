package io.jprado.api


import io.swagger.annotations.ApiParam
import io.jprado.model.dto.RentReceiptsResponseDTO
import io.jprado.model.dto.TenantDTO
import io.jprado.model.dto.TenantResponseDTO
import io.jprado.model.dto.TenantsResponseDTO
import io.jprado.service.TenantService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.hateoas.mvc.ControllerLinkBuilder
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController


@javax.annotation.Generated(value = "class io.swagger.codegen.languages.SpringCodegen", date = "2017-02-11T19:58:36.526+11:00")

@RestController
open class TenantApiController @Autowired constructor(val tenantService: TenantService) : TenantApi {

    override fun tenantPost(

            @ApiParam(value = "Payload of a new tenant.", required = true) @RequestBody tenantPayload: TenantDTO

    ): ResponseEntity<TenantResponseDTO> {
        val tenantResponseDTO = TenantResponseDTO(tenantService.create(tenantPayload))
        addLinksTenantResponseDTO(tenantResponseDTO);
        if (tenantResponseDTO.tenant == null){
            return ResponseEntity(HttpStatus.BAD_REQUEST)
        }else {
            return ResponseEntity(tenantResponseDTO,HttpStatus.OK)
        }

    }

    override fun tenantTenantIdGet(
            @ApiParam(value = "Unique id of the Tenant.", required = true) @PathVariable("tenantId") tenantId: Long?


    ): ResponseEntity<TenantResponseDTO> {
        val tenantResponseDTO = TenantResponseDTO(tenantService.findOne(tenantId))
        addLinksTenantResponseDTO(tenantResponseDTO);
        return ResponseEntity(tenantResponseDTO,HttpStatus.OK)
    }

    override fun tenantTenantIdReceiptsGet(
            @ApiParam(value = "Unique id of the Tenant.", required = true) @PathVariable("tenantId") tenantId: Long


    ): ResponseEntity<RentReceiptsResponseDTO> {
        val rentReceiptsResponseDTO = RentReceiptsResponseDTO(tenantService.rentReceipts(tenantId))
        rentReceiptsResponseDTO.add(ControllerLinkBuilder.linkTo(ControllerLinkBuilder.methodOn(TenantApiController::class.java).tenantTenantIdReceiptsGet(tenantId)).withSelfRel())
        return ResponseEntity(rentReceiptsResponseDTO, HttpStatus.OK)

    }
    override fun tenantListRentReceiptCreatedWithinHours(@ApiParam(value = "Number of last hours to search.", required = true) @RequestParam(value = "hours", required = true) hours: Long


    ): ResponseEntity<TenantsResponseDTO> {
        val tenantsResponsesDTO = TenantsResponseDTO(tenantService.findByCreatedDateTimeAfter(hours));
        if (tenantsResponsesDTO.tenantList == null) {
            return ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR)
        }else{
            tenantsResponsesDTO.add(ControllerLinkBuilder.linkTo(ControllerLinkBuilder.methodOn(TenantApiController::class.java).tenantListRentReceiptCreatedWithinHours(hours)).withSelfRel())
            return ResponseEntity(tenantsResponsesDTO, HttpStatus.OK)
        }
    }

    fun addLinksTenantResponseDTO(tenantResponseDTO: TenantResponseDTO): TenantResponseDTO{
        if (tenantResponseDTO.tenant != null && tenantResponseDTO.tenant.id != null) {
            tenantResponseDTO.add(ControllerLinkBuilder.linkTo(ControllerLinkBuilder.methodOn(TenantApiController::class.java).tenantTenantIdGet(tenantResponseDTO.tenant.id)).withSelfRel())
            tenantResponseDTO.add(ControllerLinkBuilder.linkTo(ControllerLinkBuilder.methodOn(TenantApiController::class.java).tenantTenantIdReceiptsGet(tenantResponseDTO.tenant.id)).withRel("RentReceipts"))
        }
        return tenantResponseDTO
    }
}
