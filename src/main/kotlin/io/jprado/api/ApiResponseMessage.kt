package io.jprado.api

@javax.annotation.Generated(value = "class io.swagger.codegen.languages.SpringCodegen", date = "2017-02-11T19:58:36.526+11:00")

@javax.xml.bind.annotation.XmlRootElement
class ApiResponseMessage {

    var code: Int = 0
    var type: String? = ""
    var message: String? = ""

    constructor() {}

    constructor(code: Int, message: String) {
        this.code = code
        when (code) {
            ERROR -> type = "error"
            WARNING -> type = "warning"
            INFO -> type = "info"
            OK -> type = "ok"
            TOO_BUSY -> type = "too busy"
            else -> type = "unknown"
        }
        this.message = message
    }

    companion object {
        val ERROR = 1
        val WARNING = 2
        val INFO = 3
        val OK = 4
        val TOO_BUSY = 5
    }
}
