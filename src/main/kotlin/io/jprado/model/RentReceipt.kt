package io.jprado.model

import com.fasterxml.jackson.annotation.JsonBackReference
import java.math.BigDecimal
import java.time.LocalDateTime
import javax.persistence.*


/**
 * RentReceipt
 */
@javax.annotation.Generated(value = "class io.swagger.codegen.languages.SpringCodegen", date = "2017-02-11T19:58:36.526+11:00")
@Entity
data class RentReceipt(
        @Id @GeneratedValue(strategy= GenerationType.AUTO) var id: Long? = null,
        var amount: BigDecimal,
        @ManyToOne @JoinColumn(name = "tenant_id", nullable = false) @JsonBackReference var tenant: Tenant,
        var createdDateTime: LocalDateTime
)

