package io.jprado.model.dto


import java.math.BigDecimal


/**
 * Created by jpprado on 12/2/17.
 */
@javax.annotation.Generated(value = "class io.swagger.codegen.languages.SpringCodegen", date = "2017-02-11T19:58:36.526+11:00")
data class TenantDTO constructor(
        val name: String,
        val weeklyRentAmount: BigDecimal)