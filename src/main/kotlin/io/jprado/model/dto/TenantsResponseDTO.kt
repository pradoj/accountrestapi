package io.jprado.model.dto

import io.jprado.model.Tenant
import org.springframework.hateoas.ResourceSupport

/**
 * Created by juliano.prado on 15/02/2017.
 */
class TenantsResponseDTO constructor(val tenantList: List<Tenant>?) : ResourceSupport(){
     val totalTenants = tenantList?.size ?: 0;


}