package io.jprado.model.dto

import io.jprado.model.RentReceipt
import org.springframework.hateoas.ResourceSupport

/**
 * Created by juliano.prado on 15/02/2017.
 */
class RentReceiptResponseDTO constructor(val rentReceipt: RentReceipt?) : ResourceSupport(){

}