package io.jprado.model.dto

import io.jprado.model.RentReceipt
import org.springframework.hateoas.ResourceSupport

/**
 * Created by juliano.prado on 15/02/2017.
 */
class RentReceiptsResponseDTO constructor(val rentReceipts: List<RentReceipt>?) : ResourceSupport(){
    val totalRentReceipts: Int = rentReceipts?.size ?: 0;
}