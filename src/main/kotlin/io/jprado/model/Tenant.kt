package io.jprado.model


import com.fasterxml.jackson.annotation.JsonManagedReference
import java.math.BigDecimal
import javax.persistence.*


/**
 * Tenant
 */
@javax.annotation.Generated(value = "class io.swagger.codegen.languages.SpringCodegen", date = "2017-02-11T19:58:36.526+11:00")
@Entity
class Tenant constructor(
        @Id @GeneratedValue(strategy= GenerationType.AUTO) val id: Long? = null,
        val name: String,
        @OneToMany(mappedBy = "tenant") @JsonManagedReference var rentReceipts: List<RentReceipt>? = null,
        var weeklyRentAmount: BigDecimal,
        @Column(nullable = false) var rentPaidToDate: java.time.LocalDate,
        var rentCreditAmount: BigDecimal = BigDecimal.ZERO)

