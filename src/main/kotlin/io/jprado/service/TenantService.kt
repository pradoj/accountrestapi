package io.jprado.service

import io.jprado.model.RentReceipt
import io.jprado.model.Tenant
import io.jprado.model.dto.TenantDTO
import io.jprado.repository.TenantRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.math.BigDecimal
import java.time.LocalDate


/**
 * Created by jpprado on 12/2/17.
 */
interface TenantService {
    fun rentReceipts(id: Long?): List<RentReceipt>?
    fun create(tenantPayload: TenantDTO): Tenant?
    fun findOne(tenantId: Long?) : Tenant
    fun update(tenant: Tenant) : Tenant
    fun findByCreatedDateTimeAfter(hours: Long): List<Tenant> ?
}

@Service
class TenantServiceImpl @Autowired constructor(val repository: TenantRepository ): TenantService{
    @Autowired lateinit var rentReceiptService: RentReceiptService

    override fun rentReceipts(id: Long?): List<RentReceipt>?{
        return findOne(id).rentReceipts;
    }

    override fun create(tenantPayload: TenantDTO): Tenant?{
        return repository.save(Tenant(null,tenantPayload.name,null,tenantPayload.weeklyRentAmount, LocalDate.now(), BigDecimal.ZERO))
    }

    override fun findOne(tenantId: Long?) = repository.findOne(tenantId);

    override fun update(tenant: Tenant) = repository.save(tenant);

    override fun findByCreatedDateTimeAfter(hours: Long): List<Tenant> ?{
        val rentReceiptList = rentReceiptService.findByCreatedDateTimeAfter(hours)
        return  if (rentReceiptList != null)
            rentReceiptList.map { it -> it.tenant }.distinct() else null
    }
}