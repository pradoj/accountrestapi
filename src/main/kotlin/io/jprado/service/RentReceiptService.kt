package io.jprado.service

import io.jprado.model.RentReceipt
import io.jprado.model.dto.RentReceiptCreateDTO
import io.jprado.repository.RentReceiptRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.math.BigDecimal
import java.math.RoundingMode
import java.time.LocalDateTime

/**
 * Created by jpprado on 12/2/17.
 */

interface RentReceiptService {
    fun  create(rentReceiptDTO: RentReceiptCreateDTO): RentReceipt?
    fun findByCreatedDateTimeAfter(hours: Long): List<RentReceipt>?
}

@Service
class RentReceiptServiceImpl @Autowired constructor(val repository: RentReceiptRepository): RentReceiptService{
    @Autowired lateinit var tenantService: TenantService

    override fun create(rentReceiptDTO: RentReceiptCreateDTO): RentReceipt?{
        val rentReceipt = RentReceipt(null, rentReceiptDTO.amount, tenantService.findOne(rentReceiptDTO.tenantID), LocalDateTime.now())
        val totalCreditBeforePay = BigDecimal.ZERO.plus(rentReceipt.amount).plus(rentReceipt.tenant.rentCreditAmount);
        val amountOfWeeksPaid = amountOfWeeks(rentReceipt.tenant.weeklyRentAmount,totalCreditBeforePay)
        rentReceipt.tenant.apply{
            rentPaidToDate = rentPaidToDate.plusWeeks(amountOfWeeksPaid)
            rentCreditAmount = totalCreditBeforePay.minus(BigDecimal.valueOf(amountOfWeeksPaid).times(weeklyRentAmount))
        }
        tenantService.update(rentReceipt.tenant);
        return repository.save(rentReceipt);
    }

    internal fun amountOfWeeks(weeklyRentAmount:BigDecimal, paidAmount:BigDecimal): Long{

        return paidAmount.divide(weeklyRentAmount,0,RoundingMode.DOWN).toLong();
    }

    override fun findByCreatedDateTimeAfter(hours: Long): List<RentReceipt>?{
        return repository.findByCreatedDateTimeAfter(LocalDateTime.now().minusHours(hours));
    }


}
