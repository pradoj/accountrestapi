package io.jprado.api



import io.jprado.model.Tenant
import io.jprado.model.dto.TenantDTO
import io.jprado.service.TenantService
import org.junit.Before
import org.junit.Test
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.MediaType
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter
import org.springframework.test.context.web.WebAppConfiguration
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultMatchers
import org.springframework.test.web.servlet.setup.MockMvcBuilders
import java.math.BigDecimal
import java.time.LocalDate


/**
 * Created by jpprado on 14/2/17.
 */
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@WebAppConfiguration   // 3
class TenantApiControllerIntegrationTest {
    lateinit var mockMvc: MockMvc

    @InjectMocks
    lateinit var controller: TenantApiController

    @Mock
    lateinit var tenantService: TenantService

    @Before
    fun setup(){
        MockitoAnnotations.initMocks(this)
        mockMvc = MockMvcBuilders.standaloneSetup(controller).setMessageConverters(MappingJackson2HttpMessageConverter()).build()

    }

    @Test
    fun tenantPostTest() {
        val tenantDTO = TenantDTO("John",BigDecimal(10))
        Mockito.`when`(tenantService.create(tenantDTO)).then { Tenant(1.toLong(),"John",null, BigDecimal(100),LocalDate.now()) }

        mockMvc.perform(MockMvcRequestBuilders.post("/tenant").contentType(MediaType.APPLICATION_JSON)
                .content("{\"name\": \"John\",\"weeklyRentAmount\": 10}")).andExpect(MockMvcResultMatchers.status().isOk)
    }
}
