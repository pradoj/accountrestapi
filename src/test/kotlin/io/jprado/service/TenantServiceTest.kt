package io.jprado.service


import io.jprado.model.RentReceipt
import io.jprado.repository.RentReceiptRepository
import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.Test
import org.springframework.beans.factory.annotation.Autowired
import java.math.BigDecimal
import java.time.LocalDateTime

/**
 * Created by jpprado on 14/2/17.
 */
class TenantServiceTest : ServiceSpec(){
    @Autowired
    lateinit var rentReceiptRepository: RentReceiptRepository

    @Test
    fun createTest(){
        var tenant = tenantService.findOne(3);
        assertTrue(BigDecimal.ZERO.compareTo(tenant.rentCreditAmount) == 0);
    }

    @Test
    fun findByCreatedDateTimeAfterTest(){
        rentReceiptRepository.save(RentReceipt(null, BigDecimal(300), tenantService.findOne(1), LocalDateTime.now().minusHours(5)))
        rentReceiptRepository.save(RentReceipt(null, BigDecimal(120), tenantService.findOne(2), LocalDateTime.now()))

        var tenantList = tenantService.findByCreatedDateTimeAfter(2);
        assertEquals(tenantList!!.size,1);
        assertEquals(tenantList.get(0).id,2.toLong());
        rentReceiptRepository.save(RentReceipt(null, BigDecimal(130), tenantService.findOne(2), LocalDateTime.now()))
        assertEquals(tenantList.size,1);

    }


}