package io.jprado.service

import io.jprado.model.dto.TenantDTO
import org.junit.Before
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.annotation.DirtiesContext
import org.springframework.test.context.junit4.SpringRunner
import java.math.BigDecimal

/**
 * Created by jpprado on 14/2/17.
 */
@RunWith(SpringRunner::class)
@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
@DirtiesContext
abstract class ServiceSpec {
    @Autowired
    lateinit var tenantService: TenantService

    @Before
    open fun loadTests() {
        tenantService.create(TenantDTO("John", BigDecimal(300)))
        tenantService.create(TenantDTO("Maria", BigDecimal(120.30)))
        tenantService.create(TenantDTO("Ann", BigDecimal(150.10)))
    }
}