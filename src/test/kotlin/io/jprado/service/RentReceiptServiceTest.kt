package io.jprado.service

import io.jprado.model.dto.RentReceiptCreateDTO
import org.junit.Assert
import org.junit.Test
import org.springframework.beans.factory.annotation.Autowired
import java.math.BigDecimal


/**
 * Created by jpprado on 14/2/17.
 */
class RentReceiptServiceTest : ServiceSpec(){
    @Autowired
    lateinit var rentReceiptService: RentReceiptServiceImpl

    @Test
    fun amountOfWeeksTest(){
        Assert.assertEquals(0,rentReceiptService.amountOfWeeks(BigDecimal(110.50),BigDecimal(110)))
        Assert.assertEquals(1,rentReceiptService.amountOfWeeks(BigDecimal(110.50),BigDecimal(111)))
        Assert.assertEquals(2,rentReceiptService.amountOfWeeks(BigDecimal(110.50),BigDecimal(221)))
    }

    @Test
    fun create(){
        rentReceiptService.create(RentReceiptCreateDTO(BigDecimal(50),1))
        val tenant = tenantService.findOne(1);
        rentReceiptService.create(RentReceiptCreateDTO(BigDecimal(275),1))
        val tenantUpdate = tenantService.findOne(1);
        Assert.assertEquals(tenant.rentPaidToDate.plusWeeks(1).dayOfYear,tenantUpdate.rentPaidToDate.dayOfYear)
    }
}